import Foundation

public struct Environment {
    
    enum Keys {
        enum Plist {
            static let baseURL = "BASE_URL"
            static let bundleID = "BUNDLE_ID"
            static let appID = "APP_ID"
        }
    }
    
    private static let infoDictionary: [String: Any] = {
        guard let dict = Bundle.main.infoDictionary else {
            fatalError("Plist file not found")
        }
        return dict
    }()
    
    // MARK: - Plist values
    static let baseURL: String = {
        guard let rootURLstring = Environment.infoDictionary[Keys.Plist.baseURL] as? String else {
            fatalError("Base URL not set in plist for this environment")
        }
        return rootURLstring
    }()
    
    // MARK: - Plist values
    static let bundleID: String = {
        guard let rootURLstring = Bundle.main.bundleIdentifier,!rootURLstring.isEmpty else {
            fatalError("BundleID not set in plist for this environment")
        }
        return rootURLstring
    }()
    
    // MARK: - Plist values
    static let appID: String = {
        guard let rootURLstring = Environment.infoDictionary[Keys.Plist.appID] as? String else {
            fatalError("appID not set in plist for this environment")
        }
        return rootURLstring
    }()
}
