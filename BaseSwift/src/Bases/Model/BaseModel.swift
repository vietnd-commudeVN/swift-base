import ObjectMapper

protocol BaseModelProtocol{}
class BaseModel:Mappable{
    
    init(){
        
    }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        
    }
    
}
