import UIKit

class BaseTextField: UITextField {

    @IBInspectable var maxLength:Int = 0
     
    //TODO: cần check 2 cái này
    var prohibitWords:[String] = []

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        self.commonInit()
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    func commonInit() {
        self.delegate = self
    }
    
    override var text:String?{
        didSet{
            self.setTextWith(text: text, font: self.font!)
        }
    }
}
extension BaseTextField: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        guard  textField.markedTextRange == nil ||  (textField.markedTextRange != nil && range.length > 0) else{
            return true
        }
        
        if let char = string.cString(using: String.Encoding.utf8) {
               let isBackSpace = strcmp(char, "\\b")
               if (isBackSpace == -92) {
                   print("Backspace was pressed")
                return true
               }
           }
       
        guard let currentText = textField.text,
              let rangeOfTextToReplace = Range(range, in: currentText) else {
            return false
        }

        
        let substringToReplace = currentText[rangeOfTextToReplace]
        let count = currentText.count - substringToReplace.count + string.count
        
        var updatedText = currentText.replacingCharacters(in: rangeOfTextToReplace, with: string)
        
        if self.maxLength != 0, updatedText.count > self.maxLength{
            updatedText = updatedText[0..<self.maxLength]
            textField.text = updatedText
        }
        
        return self.maxLength == 0 || count <=  self.maxLength
    }
}


extension UITextField{
    func setTextWith(text: String?, font: UIFont, lineHeight: CGFloat? = 0,
                     lineSpacing: CGFloat? = 0, characterSpacing: CGFloat? = 0,
                     breakMode: NSLineBreakMode? = .byCharWrapping, alignment: NSTextAlignment? = .left) {
        var attributes: [NSAttributedString.Key: Any] = [:]
        let paragraphStyle = NSMutableParagraphStyle()
        if let lineHeight = lineHeight,lineHeight > 0 {
            paragraphStyle.minimumLineHeight = lineHeight
            paragraphStyle.maximumLineHeight = lineHeight
        }
        paragraphStyle.lineSpacing = lineSpacing ?? 0
        paragraphStyle.lineBreakMode = breakMode ?? .byCharWrapping
        paragraphStyle.alignment = alignment ?? .left
        attributes.updateValue(paragraphStyle, forKey: .paragraphStyle)
        if let characterSpacing = characterSpacing{
            if #available(iOS 14.0, *) {
                attributes.updateValue(characterSpacing/50, forKey: NSAttributedString.Key.tracking)
            } else {
                // Fallback on earlier versions
                attributes.updateValue(characterSpacing/50, forKey: NSAttributedString.Key.kern)
            }
        }
        attributes.updateValue( font, forKey: NSAttributedString.Key.font)
        self.attributedText = NSAttributedString(string: text ?? "", attributes: attributes)
    }
}
