import UIKit
import RxSwift
import LifetimeTracker
//MARK: - BaseViewModelProtocol
protocol BaseViewModelProtocol: AnyObject {
    
    associatedtype Input
    associatedtype Output
    
    func transfrom(from input: Input) -> Output
}

//MARK: - BaseViewModel
class BaseViewModel:NSObject{
    var disposeBag = DisposeBag()
    
    override init(){
        super.init()
#if DEBUG
        trackLifetime()
#endif
    }
    deinit {
        #if DEBUG
        print("Deinit:\(self.className)")
        #endif
    }
}
