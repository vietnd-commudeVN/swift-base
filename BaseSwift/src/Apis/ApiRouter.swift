import Foundation
import RxAlamofire
import Alamofire
enum API{
}

//api path
struct Router {
}


enum ApiRouter:URLConvertible {
    case by(url:API)
    
    var path: String {
        var api:API
        switch self {
        case .by(let url):
            api = url
            break
        }
        
        let router:Router = Router()
        switch api {
        }
    }
    var baseURL:String{
        return Environment.baseURL
    }
    
    func asURL() throws -> URL {
        var url = try baseURL.asURL()
        url.appendPathComponent(path)
        print("url:\(url)")
        return url
    }
}

