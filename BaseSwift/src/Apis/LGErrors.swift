import Alamofire
public enum LGErrors:Error{
    case networkError
    case noResponse
    case responseIsInvalid
    case noStatus
    case notFound
    case unauthorized
    case systemError
    case apiErrorValidate(code:Int,errors:[String:String])
    case apiErrorString(code:Int,message:String)
    
    var localizedDescription:String{
        switch self {
        case .networkError:
            return "ネットワークが失われました。"
        case .systemError:
            return "システムエラー。"
        default:
            return ""
        }
    }
}
