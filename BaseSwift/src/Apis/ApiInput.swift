import RxAlamofire
import Alamofire

struct ApiInput{
    var method:HTTPMethod = .get
    var url:URLConvertible = ""
    var params:Parameters? = nil
    var encoding:URLEncoding = .default
    var headers:HTTPHeaders = [
//        "Accept":"application/json",
//        "Content-Type":"application/x-www-form-urlencoded",
        "platform":"ios",
//        "DEVICE_INFO": UIDevice.current.name,
//        "LANGUAGE":String(Locale.preferredLanguages[0].prefix(2))
    ]
    
    init(method:HTTPMethod, url:ApiRouter,params:Parameters? = nil,headers:[String:String]? = nil){
        self.method = method
        self.url = url
        self.params = params
        if let header = headers{
            for (name, value) in header {
                self.headers.add(name: name, value: value)
            }
        }
        let token = LGKeychainAccess.getSessionToken()
        print(token)
        if !token.isEmpty{
            self.headers.add(.authorization(bearerToken: token))
        }
        
        let dictionary = Bundle.main.infoDictionary
        guard let version = dictionary?["CFBundleShortVersionString"] as? String else{
            return
        }
        self.headers.add(name: "version", value: version)
    }
}
