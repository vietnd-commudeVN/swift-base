import Foundation
import ObjectMapper
class ApiResponse: Mappable {
    var status: Int?
    var messageFailure:String?
    var messageValidate:[String:String]?
    var data:[String:Any]?
    init(){
        
    }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        status     <- map["status"]
        messageFailure     <- map["message_failure"]
        messageValidate     <- map["message_validate"]
//        message_validate
        data   <- map["data"]
    }
    
    func isSuccess()->Bool{
        return status == 200
    }
}
