import RxAlamofire
import Alamofire
import RxSwift
import RxCocoa
import ObjectMapper
import UIKit
import Reachability
import RxReachability
class ApiRequest {
    
    class func request(_ input: ApiInput,_ showError:Bool? = false) -> Observable<ApiResponse> {
        print("params:\(input.params)")
        return Observable.create {  observer -> Disposable in
            let request = RxAlamofire.request(input.method,
                                              input.url,
                                              parameters: input.params,
                                              encoding: input.encoding,
                                              headers: input.headers)
                .validate(statusCode: 0..<1000)
                .responseJSON()
                .subscribe(on: ConcurrentDispatchQueueScheduler.init(qos: .background))
                .map {
                    response -> ApiResponse? in
                    let apiResponse = Mapper<ApiResponse>().map(JSONObject: response.value)
                    return apiResponse
                }
                .observe(on: MainScheduler.instance)
                .subscribe(
                    onNext: { response in
                     
                        guard let res = response else {
                            observer.onError(LGErrors.noResponse)
                            return
                        }
                        
                        guard res.isSuccess() else {
                            guard let status = res.status else{
                                observer.onError(LGErrors.noStatus)
                                return
                            }
                            
                            guard checkErrorStatusCode(statusCode: status,msg: res.messageFailure, url: input.url) else{
                                observer.onCompleted()
                                return
                            }
                            
                            if let msgValidate = res.messageValidate,!msgValidate.isEmpty{
                                
                                observer.onError(LGErrors.apiErrorValidate(code: status, errors: msgValidate))
                            }else if let msgFailure = res.messageFailure,!msgFailure.isEmpty{
                                observer.onError(LGErrors.apiErrorString(code: status, message: msgFailure))
                            }else{
                                observer.onError(LGErrors.systemError)
                            }
                            return
                        }
                        
                        observer.onNext(res)
                        observer.onCompleted()
                    }, onError: { (error) in
                        if !(NetworkReachabilityManager()?.isReachable ?? false) {
                            self.showError(msg: "インターネット接続を確認してください。")
                        } else {
                            self.failure(error: error)
                        }
                        observer.onError(error)
                    }
                )
            return Disposables.create([request])
        }
    }
    
    
    class func upload(_ images:[UIImage]?,_ input: ApiInput,_ showError:Bool? = false) -> Observable<ApiResponse> {
        print("params:\(input.params ?? [:])")
        return Observable.create { observer -> Disposable in
            let request =  RxAlamofire.upload(multipartFormData: { formData in
                
                if let images = images {
                    for i in 0..<images.count{
                        formData.append(images[i].jpegData(compressionQuality: 1)!, withName: "images[\(i)]", fileName: "image(\(Date()).jpg", mimeType: "image/*")
                    }
                }
                if let params = input.params {
                    for (key, value) in params {
                        
                        if let valueString = value as? String{
                            formData.append(valueString.data(using: .utf8)!, withName: key)
                        }
                        if let icon = value as? UIImage{
                            formData.append(icon.jpegData(compressionQuality: 1)!, withName: key, fileName: "\(key)(\(Date()).jpg", mimeType: "image/*")
                        }
                        if let valueArrayInt = value as? [Int]{
                            for i in 0..<valueArrayInt.count{
                                formData.append(String(valueArrayInt[i]).data(using: .utf8)!, withName: "\(key)[\(i)]")
                            }
                        }
                        
                    }
                }
            }, to: input.url, method: input.method, headers: input.headers)
                .flatMap {
                    $0.rx.responseJSON()
                }
                .map({response -> ApiResponse? in
                    let apiResponse = Mapper<ApiResponse>().map(JSONObject: response.value)
                    return apiResponse
                })
                .observe(on: MainScheduler.instance)
            
            
                .subscribe(onNext: { response in
                    
                    guard let res = response else {
                        observer.onError(LGErrors.noResponse)
                        return
                    }
                    
                    guard res.isSuccess() else {
                        guard let status = res.status else{
                            observer.onError(LGErrors.noStatus)
                            return
                        }
                        guard checkErrorStatusCode(statusCode: status,msg:res.messageFailure, url: input.url) else{
                            observer.onCompleted()
                            return
                        }
                        
                        if let msgValidate = res.messageValidate,!msgValidate.isEmpty{
                            
                            observer.onError(LGErrors.apiErrorValidate(code: status, errors: msgValidate))
                        }else if let msgFailure = res.messageFailure,!msgFailure.isEmpty{
                            observer.onError(LGErrors.apiErrorString(code: status, message: msgFailure))
                        }else{
                            observer.onError(LGErrors.systemError)
                        }
                        return
                    }
                    
                    observer.onNext(res)
                    observer.onCompleted()
                }, onError: { (error) in
                    if !(NetworkReachabilityManager()?.isReachable ?? false) {
                        self.showError(msg: "インターネット接続を確認してください。")
                    } else {
                        self.failure(error: error)
                    }
                    observer.onError(error)
                }
                )
            return Disposables.create([request])
        }
    }
    
    private class func checkErrorStatusCode(statusCode:Int,msg:String?,url:URLConvertible)->Bool{
        switch statusCode {
        case 401: // unauthorized
            LGKeychainAccess.logout()
            LGUserDefaults.logout()
            return false
        case 404: //not found
    
            return false
        case 406: //Account lock

            return false
        case 407: //Maintenance
            
            return false
        case 408: //update version
            return false
        default:
            break
        }
        return true
    }
    
    private class func failure(error:Error){
        if let error = error.asAFError {
            switch error {
            case .sessionTaskFailed(let err):
                self.showError(msg: "サーバへのリクエストがタイムアウトしました。")
            default:
                self.showError(msg:  error.localizedDescription)
            }
        }
        
    }
    
    
    private class func showError(msg:String,callback:(()->Void)? = nil){
        NSLog("Error:\(msg)")
        DispatchQueue.main.async {
           
        }
    }
}


