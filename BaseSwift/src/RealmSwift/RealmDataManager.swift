import Foundation
import RealmSwift
class RealmDataManager: NSObject {
    
    var realm: Realm!
    static let shared = RealmDataManager()
    
    override init() {
        var config = Realm.Configuration.defaultConfiguration
        config.schemaVersion = 5
        config.migrationBlock = { _, oldSchemaVersion in
            if (oldSchemaVersion < 5) {

            }
        }
        realm = try? Realm(configuration: config)
        print("RealmURL:\(Realm.Configuration.defaultConfiguration.fileURL)")
        
    }

    // MARK: common functions
    @discardableResult
    func addObject(object: Object) -> Object {
        try? realm.write {
            realm.add(object)
        }
        return object
    }
    func delete(object: Object) {
        try? realm.write {
            realm.delete(object)
        }
    }
    func deleteAll(except types: Object.Type...) {
        try? realm.write {
            realm.configuration.objectTypes?.filter{ type in types.contains{ $0 == type } == false}.forEach { objectType in
                if let type = objectType as? Object.Type {
                    realm.delete(realm.objects(type))
                }
            }
        }
    }
}
