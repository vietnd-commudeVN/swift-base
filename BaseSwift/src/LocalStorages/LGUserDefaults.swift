import UIKit

class LGUserDefaults: NSObject {
    static let KEY_ = "KEY_"
    static let KEY_FCM_TOKEN = "KEY_FCM_TOKEN"
    static let KEY_TYPE_LOGIN = "KEY_TYPE_LOGIN"
    static let userDefaults = UserDefaults.standard
    class func logout(){
        userDefaults.set(nil, forKey: LGUserDefaults.KEY_TYPE_LOGIN)
        userDefaults.synchronize()
    }
    class func withdraw(){
        logout()
    }
    
    class func set(value: Any?, key:String){
        userDefaults.setValue(value, forKey: key)
        userDefaults.synchronize()
    }
    class func getString(key:String)-> String{
        let value = userDefaults.string(forKey: key)
        return (value != nil) ? value! : ""
    }
   
    
    //Access token
    class func set_(value:String){
        self.set(value: value, key: LGUserDefaults.KEY_)
    }
    class func get_()->String{
        return self.getString(key: LGUserDefaults.KEY_)
    }
}
