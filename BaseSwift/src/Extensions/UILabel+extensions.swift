import UIKit
extension UILabel{
    
    func setTextWith(text: String?, font: UIFont, lineHeight: CGFloat? = 0, lineSpacing: CGFloat? = 0, characterSpacing: CGFloat? = 0, breakMode: NSLineBreakMode? = .byCharWrapping, alignment: NSTextAlignment? = .left,lineHeightMultiple: CGFloat? = 0) {
        var attributes: [NSAttributedString.Key: Any] = [:]
        let paragraphStyle = NSMutableParagraphStyle()
        if let lineHeight = lineHeight,lineHeight > 0 {
            paragraphStyle.minimumLineHeight = lineHeight
            paragraphStyle.maximumLineHeight = lineHeight
        }
        if let lineHeightMultiple = lineHeightMultiple,lineHeightMultiple > 0 {
            paragraphStyle.lineHeightMultiple = lineHeightMultiple
        }
        
        paragraphStyle.lineSpacing = lineSpacing ?? 0
        paragraphStyle.lineBreakMode = breakMode ?? self.lineBreakMode
        paragraphStyle.alignment = alignment ?? self.textAlignment
        attributes.updateValue(paragraphStyle, forKey: .paragraphStyle)
        if let characterSpacing = characterSpacing{
            if #available(iOS 14.0, *) {
                attributes.updateValue(characterSpacing/50, forKey: NSAttributedString.Key.tracking)
            } else {
                // Fallback on earlier versions
                attributes.updateValue(characterSpacing/50, forKey: NSAttributedString.Key.kern)
            }
        }
        attributes.updateValue( font, forKey: NSAttributedString.Key.font)
        self.attributedText = NSAttributedString(string: text ?? "", attributes: attributes)
    }
    func getLinesArrayFromLabel() -> [String] {
      let text:NSString = self.text! as NSString // TODO: Make safe?
      let font:UIFont = self.font
      let rect:CGRect = self.frame
        
      let myFont:CTFont = CTFontCreateWithName(font.fontName as CFString, font.pointSize, nil)
      let attStr:NSMutableAttributedString = NSMutableAttributedString(string: text as String)
        attStr.addAttribute(NSAttributedString.Key(rawValue: String(kCTFontAttributeName)), value:myFont, range: NSMakeRange(0, attStr.length))
      let frameSetter:CTFramesetter = CTFramesetterCreateWithAttributedString(attStr as CFAttributedString)
      let path:CGMutablePath = CGMutablePath()
      path.addRect(CGRect(x:0, y:0, width:rect.size.width, height:100000))
        
      let frame:CTFrame = CTFramesetterCreateFrame(frameSetter, CFRangeMake(0, 0), path, nil)
      let lines = CTFrameGetLines(frame) as NSArray
      var linesArray = [String]()
        
      for line in lines {
        let lineRange = CTLineGetStringRange(line as! CTLine)
        let range:NSRange = NSMakeRange(lineRange.location, lineRange.length)
        let lineString = text.substring(with: range)
        linesArray.append(lineString as String)
      }
      return linesArray
    }
}

extension String {
    func height(forConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(
            with: constraintRect,
            options: [.usesLineFragmentOrigin, .usesFontLeading],
            attributes: [.font: font],
            context: nil
        )
        return boundingBox.height
    }
}
