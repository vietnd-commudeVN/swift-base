import CoreLocation
import Contacts
extension CLGeocoder {
    
    struct Address {
        var prefecture: String? // 都道府県 例) 東京都
        var city: String? // 市区町村 例) 墨田区
        // 地名 例) 押上
    }
    func getAddressFromCoordinate(from coordinate:CLLocationCoordinate2D, completion: @escaping (Address?,String?, Error?) -> Void) {
        if self.isGeocoding{
            self.cancelGeocode()
        }
        let location = CLLocation(
            latitude: coordinate.latitude,
            longitude: coordinate.longitude
        )
        self.reverseGeocodeLocation(location,preferredLocale: Locale.init(identifier: "ja_JP")) { placemarks, error in
            guard let placemark = placemarks?.first, error == nil else {
                completion(nil,nil, error)
                return
            }
            print(placemark.addressDictionary)
            var address: Address = Address()
            address.prefecture = placemark.administrativeArea
            address.city = placemark.locality
            guard let countryCode = placemark.addressDictionary!["CountryCode"] as? String else{return }
            completion(address,countryCode, nil)
        }
    }
}
