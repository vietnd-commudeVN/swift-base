import UIKit

extension NSMutableAttributedString {
    func setAsLink(textToFind:String, linkName:String) {
        let foundRange = self.mutableString.range(of: textToFind)
        if foundRange.location != NSNotFound {
            self.addAttribute(NSAttributedString.Key.link, value: linkName, range: foundRange)
        }
    }
    
}

extension String{
    func toDouble()-> Double{
        let formatter = NumberFormatter()
        formatter.locale = Locale.init(identifier: "ja_JP")
        formatter.numberStyle = .decimal
        let number = formatter.number(from: self)
        if let value = number as? Double{
            return value
        }
        return 0
    }
    
    static func stringWithPattern(pattern:String,value:String?)->String{
        return String.init(format: NSLocalizedString(pattern, comment: ""), NSLocalizedString(value ?? "" , comment: "") )
    }
    
    var trimText:String{
        get{
            return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        }
    }
}
extension String{
    func phoneFormatted() -> String {
        let cleanNumber = self.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        let format: [Character] = ["X", "X", "X", "-", "X", "X", "X", "X","-", "X", "X", "X", "X"]
        
        var result = ""
        var index = cleanNumber.startIndex
        for ch in format {
            if index == cleanNumber.endIndex {
                break
            }
            if ch == "X" {
                result.append(cleanNumber[index])
                index = cleanNumber.index(after: index)
                
            } else {
                result.append(ch)
            }
        }
        return result
    }
}
extension String{
    func isSatifyRegex(pattern:String)->Bool{
        let test = NSPredicate(format:"SELF MATCHES %@", pattern)
        return test.evaluate(with: self)
    }
    
    func checkByLuhnAlgorithm()-> Bool{
        var sum = 0
        let reversedCharacters = self.reversed().map { String($0) }
        for (idx, element) in reversedCharacters.enumerated() {
            guard let digit = Int(element) else { return false }
            switch ((idx % 2 == 1), digit) {
            case (true, 9): sum += 9
            case (true, 0...8): sum += (digit * 2) % 9
            default: sum += digit
            }
        }
        return sum % 10 == 0
    }
}

extension String {
    public var halfWidth: String {
        let text: CFMutableString = NSMutableString(string: self) as CFMutableString
        CFStringTransform(text, nil, kCFStringTransformFullwidthHalfwidth, false)
        return text as String
    }
    public var fullWidth: String {
        let text: CFMutableString = NSMutableString(string: self) as CFMutableString
        CFStringTransform(text, nil, kCFStringTransformFullwidthHalfwidth, true)
        return text as String
    }
    
    public var fromHiraToKana: String {
        let text: CFMutableString = NSMutableString(string: self) as CFMutableString
        CFStringTransform(text, nil, kCFStringTransformHiraganaKatakana, false)
        return text as String
    }
    public var fromKanjiToKana: String {
        let text: CFMutableString = NSMutableString(string: self) as CFMutableString
        CFStringTransform(text, nil, kCFStringTransformLatinKatakana, false)
        return text as String
    }
}
extension String{
    func estimatedLabelHeight(width: CGFloat,font:UIFont) -> CGFloat {
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 4
        style.lineBreakMode = NSLineBreakMode.byWordWrapping
        
        let attributedText = [
            NSAttributedString.Key.font: font,
            NSAttributedString.Key.paragraphStyle: style
        ] as [NSAttributedString.Key : Any]
        let mutableAttributedString = NSMutableAttributedString.init(string:self, attributes: attributedText)
        return mutableAttributedString.estimatedLabelHeight(width: width)
    }
    
    func estimatedLabelWidth(height: CGFloat,font:UIFont) -> CGFloat {
        let attributedText = [
            NSAttributedString.Key.font: font
        ] as [NSAttributedString.Key : Any]
        let mutableAttributedString = NSMutableAttributedString.init(string:self, attributes: attributedText)
        return mutableAttributedString.estimatedLabelWidth(height: height)
    }
}
extension NSMutableAttributedString{
    func estimatedLabelHeight(width: CGFloat) -> CGFloat {
        
        let size = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        let options = NSStringDrawingOptions.usesLineFragmentOrigin
        
        let rectangleHeight = self.boundingRect(with: size, options: options, context: nil).height
        
        return rectangleHeight
    }
    
    func estimatedLabelWidth(height: CGFloat) -> CGFloat {
        
        let size = CGSize(width: CGFloat.greatestFiniteMagnitude, height: height)
        let options = NSStringDrawingOptions.usesLineFragmentOrigin
        
        let rectangleWidth = self.boundingRect(with: size, options: options, context: nil).width
        
        return rectangleWidth
    }
    
}


private extension CFStringTokenizer {
    var hiragana: String { string(to: kCFStringTransformLatinHiragana) }
    var katakana: String { string(to: kCFStringTransformHiraganaKatakana) }
    
    private func string(to transform: CFString) -> String {
        var output: String = ""
        while !CFStringTokenizerAdvanceToNextToken(self).isEmpty {
            output.append(letter(to: transform))
        }
        return output
    }
    
    private func letter(to transform: CFString) -> String {
        let mutableString: NSMutableString =
        CFStringTokenizerCopyCurrentTokenAttribute(self, kCFStringTokenizerUnitWord)
            .flatMap { $0 as? NSString }
            .map { $0.mutableCopy() }
            .flatMap { $0 as? NSMutableString } ?? NSMutableString()
        CFStringTransform(mutableString, nil, transform, false)
        return mutableString as String
    }
}

enum Kana { case hiragana, katakana }

func convert(_ input: String, to kana: Kana) -> String {
    let trimmed: String = input.trimmingCharacters(in: .whitespacesAndNewlines)
    let tokenizer: CFStringTokenizer =
    CFStringTokenizerCreate(kCFAllocatorDefault,
                            trimmed as CFString,
                            CFRangeMake(0, trimmed.utf16.count),
                            kCFStringTokenizerUnitWordBoundary,
                            Locale(identifier: "ja") as CFLocale)
    switch kana {
    case .hiragana: return tokenizer.hiragana
    case .katakana: return tokenizer.katakana
    }
}

extension String {
    var hiragana: String { convert(self, to: .hiragana) }
    var katakana: String { convert(self, to: .katakana) }
}

extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return nil
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}

extension String {
    subscript(_ range: CountableRange<Int>) -> String {
        let start = index(startIndex, offsetBy: max(0, range.lowerBound))
        let end = index(start, offsetBy: min(self.count - range.lowerBound,
                                             range.upperBound - range.lowerBound))
        return String(self[start..<end])
    }
    
    subscript(_ range: CountablePartialRangeFrom<Int>) -> String {
        let start = index(startIndex, offsetBy: max(0, range.lowerBound))
        return String(self[start...])
    }
    func removeString() -> String {
        if self[0..<1] == "0"{
            let a = self.replacingOccurrences(of: "0", with: "")
            return a
        }else{
            return self
        }
    }
}
