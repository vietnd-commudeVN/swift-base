import Foundation
import UIKit

extension Date{
    var year:Int{
        get{
            let calendar = Calendar.init(identifier: .gregorian)
            let components: DateComponents? = calendar.dateComponents([.year], from: self)
            return components?.year ?? 0
        }
    }
    var month:Int{
        get{
            let calendar = Calendar.init(identifier: .gregorian)
            let components: DateComponents? = calendar.dateComponents([.month], from: self)
            return components?.month ?? 0
        }
    }
    
    var day:Int{
        get{
            let calendar = Calendar.init(identifier: .gregorian)
            let components: DateComponents? = calendar.dateComponents([.day], from: self)
            return components?.day ?? 0
        }
    }
    
    static func  dateFromComponents(year:Int, month:Int,day:Int)->Date?{
        var calendar = Calendar.init(identifier: .gregorian)
        calendar.locale = Locale.current
        calendar.timeZone = TimeZone.current
        var components: DateComponents? = calendar.dateComponents([.year, .month, .day], from: Date())
        components?.day = day
        components?.month = month
        components?.year = year
        
        if let components = components{
            let date = calendar.date(from: components)
            return date
        }
        return nil
    }
    
    static func dateFromString(dateString:String,format:String)->Date?{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.locale = Locale.current
        dateFormatter.timeZone = TimeZone.current
        return dateFormatter.date(from:dateString)
    }
    func dateToString(format:String)-> String{
       
        let dateFormatter = DateFormatter()

        dateFormatter.dateFormat = format
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.locale = Locale.current
        dateFormatter.timeZone = TimeZone.current
        return dateFormatter.string(from: self)
    }
    func stringWith(format:String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.locale = Locale(identifier: "ja_JP")
        dateFormatter.timeZone = TimeZone.current
        return dateFormatter.string(from: self)
    }
    
    func getAgeFromDOF() -> (year:Int,month:Int,daty:Int) {
        
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "YYYY-MM-dd"
        let calender = Calendar.init(identifier: .gregorian)
        let dateComponent = calender.dateComponents([.year, .month, .day], from:
                                                        self, to: Date())
        
        return (dateComponent.year!, dateComponent.month!, dateComponent.day!)
    }
    
    public func timeAgoSince() -> String {
        var calendar = Calendar(identifier: .gregorian)
        calendar.timeZone = TimeZone.current
        let dateFormat = "yyyy年M月d日"
        let thisYearDateFormat = "M月d日"
        let now = Date()
        let unitFlags: NSCalendar.Unit = [.second, .minute, .hour, .day]
        let components = (calendar as NSCalendar).components(unitFlags, from: self, to: now, options: [])

        if  let day = components.day, day >= 8 {
            if now.year == self.year {
                return self.stringWith(format: thisYearDateFormat)
            }
            return self.stringWith(format: dateFormat)
        }
        if let day = components.day, day >= 1 {
                   return "\(day)日前"
               }

        if let hour = components.hour, hour >= 1 {
            return "\(hour)時間前"
        }

        if let minute = components.minute, minute >= 1 {
            return "\(minute)分前"
        }
        return "今"

    }
    public func minutesAgoSince() -> String {
        var calendar = Calendar(identifier: .gregorian)
        calendar.timeZone = TimeZone.current
        let now = Date()
        let unitFlags: NSCalendar.Unit = [.second, .minute, .hour, .day]
        let components = (calendar as NSCalendar).components(unitFlags, from: self, to: now, options: [])

        if let hour = components.hour, hour >= 1 {
            return self.dateStringWith(strFormat: "H:mm")
        }

        if let minute = components.minute, minute >= 1 {
            return "\(minute)分前"
        }
        return "今"

    }
    func localDate() -> Date {
        let timeZoneOffset = Double(TimeZone.current.secondsFromGMT(for: self))
        guard let localDate = Calendar.current.date(byAdding: .second, value: Int(timeZoneOffset), to: self) else {return Date()}
        return localDate
    }
    func localDateReduce() -> Date {
        let timeZoneOffset = -Double(TimeZone.current.secondsFromGMT(for: self))
        guard let localDate = Calendar.current.date(byAdding: .second, value: Int(timeZoneOffset), to: self) else {return Date()}
        return localDate
    }
}
extension Date {
    func dateStringWith(strFormat: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.calendar = Calendar(identifier: .gregorian)
        dateFormatter.locale = Locale.current
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = strFormat
        return dateFormatter.string(from: self)
    }
}
enum DateRoundingType {
    case round
    case ceil
    case floor
}

extension Date {
    func rounded(minutes: TimeInterval, rounding: DateRoundingType = .round) -> Date {
        return rounded(seconds: minutes * 60, rounding: rounding)
    }
    func rounded(seconds: TimeInterval, rounding: DateRoundingType = .round) -> Date {
        var roundedInterval: TimeInterval = 0
        switch rounding  {
        case .round:
            roundedInterval = (timeIntervalSinceReferenceDate / seconds).rounded() * seconds
        case .ceil:
            roundedInterval = ceil(timeIntervalSinceReferenceDate / seconds) * seconds
        case .floor:
            roundedInterval = floor(timeIntervalSinceReferenceDate / seconds) * seconds
        }
        return Date(timeIntervalSinceReferenceDate: roundedInterval)
    }
    static func - (lhs: Date, rhs: Date) -> TimeInterval {
        return lhs.timeIntervalSinceReferenceDate - rhs.timeIntervalSinceReferenceDate
    }
}
