import UIKit
import Photos
extension UIImage {
    
    convenience init?(barcode: String) {
        let data = barcode.data(using: .ascii)
        guard let filter = CIFilter(name: "CICode128BarcodeGenerator") else {
            return nil
        }
        filter.setValue(data, forKey: "inputMessage")
        filter.setValue(0.0, forKey:"inputQuietSpace")

        let scale = UIScreen.main.scale
        let transform = CGAffineTransform(scaleX: scale, y: scale)
        guard var ciImage = filter.outputImage?.transformed(by: transform) else { return nil }
        
        let imageSize = ciImage.extent.integral
        var width:CGFloat = 0
        if UIDevice.current.userInterfaceIdiom == .phone{
            width = SCREEN_WIDTH - 80
        }else{
            width = 375
        }
        let outputSize = CGSize(width:width, height: 78)
        ciImage = ciImage.transformed(by:CGAffineTransform(scaleX: outputSize.width/imageSize.width, y: outputSize.height/imageSize.height))
        
        let colorParameters = [
            "inputColor0": CIColor(color: UIColor.black), // Foreground
            "inputColor1": CIColor(color: UIColor.clear) // Background
        ]
        let colored = ciImage.applyingFilter("CIFalseColor", parameters: colorParameters)
        
        //        return UIImage(ciImage: colored)
        self.init(ciImage: colored)
        let context:CIContext = CIContext.init(options: nil)
        let cgImage:CGImage = context.createCGImage(colored, from: colored.extent)!
        self.init(cgImage: cgImage)
    }
    
    func convertCIImageToUIImage(ciimage:CIImage)->UIImage{
        let context:CIContext = CIContext.init(options: nil)
        let cgImage:CGImage = context.createCGImage(ciimage, from: ciimage.extent)!
        let image:UIImage = UIImage.init(cgImage: cgImage)
        return image
    }
    
    
    func toBase64Png() -> String? {
        guard let imageData = self.pngData() else { return nil }
        return imageData.base64EncodedString()
    }
    func toBase64Jpeg() -> String? {
        guard let imageData = self.jpegData(compressionQuality: 0.1) else { return nil }
        return imageData.base64EncodedString()
    }
    
}

extension UIButton {
    
    /// Add image on left view
    func leftImage(image: UIImage) {
        self.setImage(image, for: .normal)
        self.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: image.size.width)
    }
}
extension PHAsset {
    
    // MARK: - Public methods
    
    func getAssetThumbnail(size: CGSize) -> UIImage {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        var thumbnail = UIImage()
        option.isSynchronous = true
        manager.requestImage(for: self, targetSize: size, contentMode: .aspectFill, options: option, resultHandler: {(result, info)->Void in
            thumbnail = result!
        })
        
        return thumbnail
    }
    
    func getOrginalImage(complition:@escaping (UIImage) -> Void) {
        let manager = PHImageManager.default()
        let option = PHImageRequestOptions()
        var image = UIImage()
        manager.requestImage(for: self, targetSize: PHImageManagerMaximumSize, contentMode: .default, options: option, resultHandler: {(result, info)->Void in
            image = result!
            
            complition(image)
        })
    }
    
    func getImageFromPHAsset(mode: PHImageRequestOptionsDeliveryMode = .highQualityFormat) -> UIImage {
        var image = UIImage()
        let requestOptions = PHImageRequestOptions()
        requestOptions.resizeMode = PHImageRequestOptionsResizeMode.exact
        requestOptions.deliveryMode = mode
        requestOptions.isSynchronous = true
        requestOptions.isNetworkAccessAllowed = true
        if (self.mediaType == PHAssetMediaType.image) {
            PHImageManager.default().requestImage(for: self, targetSize: CGSize(width: 1000, height: 1000), contentMode: .default, options: requestOptions, resultHandler: { (pickedImage, info) in
                image = pickedImage ?? UIImage()
            })
        }
        return image
    }
    
}
