import UIKit

extension UITableViewCell {
    static var nib:UINib {
        get{
           return UINib.init(nibName: self.className, bundle: nil)
        }
    }
    static var reuseIdentifier:String{
        get{
            return self.className
        }
    }
}

extension UITableViewHeaderFooterView {
    static var nib:UINib {
        get{
           return UINib.init(nibName: self.className, bundle: nil)
        }
    }
    static var reuseIdentifier:String{
        get{
            return self.className
        }
    }
}
