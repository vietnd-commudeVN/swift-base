import Foundation
import UIKit

func validatePassword(oldPass:String?,newPass:String?,newPassAgain:String?)->(result:Bool,errorMessage:String?) {

        var message = ""
        if oldPass != "12345678" {
            message = "密码错误"
        } else if newPass == "" {
            message = "没有输入新密码"
        } else if newPass!.count < 8 {
            message = "需要 8 个字符或更多字符的新密码"
        } else if oldPass == newPass {
            message = "新密码与旧密码相同"
        } else if newPassAgain == "" {
            message = "没有重新输入新密码"
        } else if newPass != newPassAgain {
            message = "重新输入新的错误密码"
        } else if oldPass == "12345678" && oldPass != newPass && newPass == newPassAgain {
            message = "修改密码成功"
        }
        return (true, message)
    }

func loginPassword(input:String?)->(result:Bool,errorMessage:String?) {
    var message = ""
    if input == "12345678" {
        message = "登录成功"
    } else {
        message = "错误的"
    }
    return (true,message)
}
