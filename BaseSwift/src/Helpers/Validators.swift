import Foundation
import UIKit
class Validators {
    static func validatePassword(password:String?,label:String? = nil)->(result:Bool,errorMessage:String?) {
        let passwordRegex =  "[A-Za-z\\d@$!%*#?&^_-]+"
        let passwordCheck = NSPredicate(format: "SELF MATCHES %@", passwordRegex)
        if password?.isEmpty ?? true {
            return (false,"入力いただいた現在のパスワードが違います")
        }
        if password!.count < 8 {
            return (false,"８文字以上を入力してください。")
        } else if passwordCheck.evaluate(with: password) == false {
            return (false,"有効なパスワードアドレスを指定してください")
        }
        return (true, nil)
    }
    static func validateEmail(email:String?,label:String? = nil)->(result:Bool,errorMessage:String?) {
        let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailCheck = NSPredicate(format: "SELF MATCHES %@", emailRegex)
        if email == nil || email == "" {
            return (false,"必要とされている")
        }
        if  emailCheck.evaluate(with: email) == false {
            return (false,"有効なメールアドレスを指定してください ")
        }
        return (true, nil)
    }

    static func validateNewPassword(newPassword:String?,oldPassword:String?,newPassLabel:String? = nil, oldPassLabel:String? = nil)->(result:Bool,errorMessage:String?) {
        let (result,errorMessage) = validatePassword(password: newPassword,label: "\(newPassLabel ?? "")")
        if !result {
            return (false,errorMessage)
        }
        if newPassword == oldPassword,oldPassword != nil {
            return (false, "新しいパスワードは古いパスワードとは異なる必要があります")
        }
        return (true,nil)
    }

    static func validateConfirmPassword(confirmPassword:String?,newPassword: String?,confirmPassLabel:String? = nil,newPassLabel:String? = nil)->(result:Bool,errorMessage:String?) {
        if confirmPassword == nil || confirmPassword == "" {
            return (false, "新しいパスワードを再入力していません")
        } else if confirmPassword != newPassword || confirmPassword!.count < 8 {
            return (false,"新パスワードと再入力パスワードが一致しません")
        }
        return (true,nil)
    }

    static func validatePhoneNumber(phoneNumber:String?, errorMessage:String? = nil)->(result:Bool,errorMessage:String?) {
        let phoneRegex = "\\d{3}-\\d{4}-\\d{3,4}"
        let phoneCheck = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
        if phoneNumber == nil || phoneNumber == "" {
            return (false,"電話番号を入力してください")
        } else if phoneCheck.evaluate(with: phoneNumber) == false {
            return (false,"10〜11文字しかできませんでした")
        }
        return (true,nil)
    }

    static func validateTextView(text:String?,errorMessage:String? = nil)->(result:Bool,errorMessage:String?) {
        if text == nil || text == "" {
            return (false,"お問い合わせ内容を入力して下さい。")
        } else if text!.count  > 1000 {
            return (false,"制限文字数を超えています。")
        }
        return (true,nil)
    }
}
